var gulp = require('gulp'),
  stylus = require('gulp-stylus'),
  plumber = require('gulp-plumber'),
  source = require('vinyl-source-stream'),
  buffer = require('vinyl-buffer'),
  browserify = require('browserify'),
  watchify = require('watchify');

var scriptsDir = __dirname + '/wp-content/themes/blank/js';
var stylDir = __dirname + '/wp-content/themes/blank/styl';
var cssDir = __dirname + '/wp-content/themes/blank/css';

function style () {
  return gulp.src(stylDir + '/main.styl')
    .pipe(plumber())
    .pipe(stylus())
    .pipe(gulp.dest(cssDir));
}

function scripts (watch) {
  var opts = {
    fullPaths: true,
    paths: [scriptsDir]
  };
  
  var bundler = browserify(scriptsDir + '/src/init.js', opts);

  if (watch) {
    bundler = watchify(bundler);
  }

  var rebundle = function () {
    console.log('Rebuilding scripts');

    bundler
      .bundle()
      .on('error', console.log)
      .pipe(source('app.js'))
      .pipe(buffer())
      .pipe(gulp.dest(scriptsDir));
  };

  bundler.on('update', rebundle);
  rebundle();

  return bundler;
}

gulp.task('scripts', function () {
  return scripts();
});

gulp.task('style', style);

gulp.task('build', ['scripts', 'style']);

gulp.task('watch', ['style'], function () {
  gulp.watch(stylDir + '/**.styl', ['style']);
  scripts(true);
});