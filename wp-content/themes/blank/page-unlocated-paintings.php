<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

  <?php
    $opts = array(
      'post_type' => 'artwork',
      'tax_query' => array(
        array(
          'taxonomy' => 'type',
          'field' => 'slug',
          'terms' => array('unlocated-paintings')
        )
      )
    );
    $wp_query = new WP_Query($opts);
  ?>

  <?php
    // Pass query var to artwork grid partial so that type is set correctly
    // in links to full images
    set_query_var('artwork_query_args', array('type' => 'unlocated-paintings'));
  ?>
    <?php get_template_part('partials/artwork-grid'); ?>
  <?php wp_reset_query(); ?>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
