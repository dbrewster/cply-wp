<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

  <?php query_posts('post_type=sms-image'); ?>
    <?php get_template_part('partials/artwork-grid'); ?>
  <?php wp_reset_query(); ?>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
