<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>
  <div class="exhibition">

    <div class="exhibition__images">
      <?php foreach (get_field('gallery') as $image) { ?>
        <?php       
          $width = $image['sizes']['large-width'];
          $height = $image['sizes']['large-height'];
          $padding = $height / $width * 100 . '%';
        ?>

        <div class="exhibition__image">
          <div class="exhibition__image__inner" style="padding-bottom: <?= $padding; ?>">
            <img src="<?= $image['sizes']['large']; ?>">
          </div>

          <?php if ($image['caption']) { ?>
            <div class="exhibition__image__caption">
              <?= $image['caption']; ?>
            </div>
          <?php } ?>
        </div>
      <?php } ?>
    </div>

    <div class="exhibition__meta">
      <?php
        $exhibitor = get_field('exhibitor');
        $location = get_field('location');
        $year = get_field('year');
      ?>

      <?php the_title(); ?>
      <?php if ($exhibitor) { ?>
        <br><?= $exhibitor ?>
      <?php } ?>        
      <?php if ($location) { ?>
        <br><?= $location ?>
      <?php } ?>       
      <?php if ($year) { ?>
        <br><?= $year ?>
      <?php } ?>
    </div>

    <?php if (get_the_content() && get_the_content() != '') { ?>
      <div class="exhibition__text">
        <?php the_content(); ?>
      </div>
    <?php } ?>
  </div>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
