<?php 
  if (get_post_type() == 'artwork') {
    $previousUrl = get_offset_artwork_permalink(-1);
    $nextUrl = get_offset_artwork_permalink(1);
  } else {
    $next = get_previous_post();
    $previous = get_next_post();

    if ($next) {
      $nextUrl =  get_permalink($next);
    }

    if ($previous) {
      $previousUrl = get_permalink($previous);
    }
  }

  if (empty($nextUrl)) { 
    $nextUrlClass = 'is-inactive';
  } else {
    $nextUrlClass = '';
  }

  if (empty($previousUrl)) { 
    $previousUrlClass = 'is-inactive';
  } else {
    $previousUrlClass = '';
  }
?>

<?php get_header(); ?>

<div class="lightbox pre-mouse-move">

  <div class="lightbox__inner">
    <div class="lightbox__content">
    
      <div class="lightbox__nav-links">
        <div class="lightbox__prev_link lightbox__nav_link">
          <a class="<?= $previousUrlClass; ?>" href="<?= $previousUrl; ?>">Prev</a>
        </div>

        <div class="lightbox__next_link lightbox__nav_link">
          <a class="<?= $nextUrlClass; ?>" href="<?= $nextUrl; ?>">Next</a>
        </div>
      </div>
 
      <div class="lightbox__item lightbox__item--current">
        <?php
          $image = get_field('image');
          $aspectRatio = $image['width'] / $image['height'];
          $aspectType = $aspectRatio > 1 ? 'landscape' : 'portrait';
        ?>
        <img src="<?= $image['sizes']['large']; ?>" class="lightbox__item__image lightbox__item__image--<?= $aspectType; ?>">
      </div>

      <?
        $type = get_query_var('type');

        if ('artwork' == get_post_type()) {
          if ($type == 'unlocated-paintings') {
            $closeUrl = site_url() . '/catalogue-raisonne/unlocated-paintings';
          } else {
            $closeUrl = site_url() . '/artwork';
            $closeUrl = add_query_arg('type', $type, $closeUrl);
          }
        } else if ('copley-images' == get_post_type()) {
          $closeUrl = site_url() . '/about/biography';
        } else if ('sms-image' == get_post_type()) {
          $closeUrl = site_url() . '/copley-galleries/sms-images';
        }
      ?>
    </div>

    <a class="lightbox__close_link" href="<?= $closeUrl ?>">Close</a>
  </div>

  <div class="lightbox__caption">
    <div class="lightbox__title"><?php the_title(); ?></div>
    <?php get_template_part('partials/artwork-caption'); ?>
  </div>
</div>

<?php get_footer(); ?>
