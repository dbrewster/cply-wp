<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

  <div class="page-content">
    <?php the_content(); ?>

    <div class="estate__attribution">
      Design by <a href="http://beautiful-company.com">Beatiful Company</a><br>
      Development by <a href="http://danielbrewster.com">Dan Brewster</a>
    </div>
  </div>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>


