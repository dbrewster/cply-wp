<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

  <div class="news-item">
    <div class="news-item__image">
      <?php the_post_thumbnail('large'); ?>
      <div class="news-item__image__caption">
        <?php 
          $credit = get_field('credit');
          $thumbnail_id = get_post_thumbnail_id($post->ID);
          $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
        ?>
        <?= $thumbnail_image[0]->post_excerpt; ?>
      </div>
    </div>

    <div class="news-item__meta">
      <div class="news-item__title"><?php the_title(); ?></div>
      <div class="news-item__date">
        <?php the_date(); ?><?php if (!empty($credit)) { ?>.<?php } ?>
        <?= get_field('credit') ?>
      </div>
    </div>

    <div class="news-item__content">
      <?php the_content();  ?>
    </div>
  </div>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
