<?php get_header(); ?>

<div class="exhibitions-content">
  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <?php 
    $index = $wp_query->current_post; 
    $thumb_id = get_post_thumbnail_id($post->ID);
    $img = wp_get_attachment_image_src($thumb_id, 'large');
    $width = $img[1];
    $height = $img[2];
    $padding = $height / $width * 100 . '%';
  ?>
    <div class="exhibition-summary <?php if (($index + 1) % 2 == 0) { echo 'clear-2'; } ?>">
      <div class="exhibition-summary__image image-preview">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" style="padding-bottom:<?= $padding ?>">
          <?php the_post_thumbnail('large'); ?>
        </a>
      </div>
      <div class="exhibition-summary__meta">
        <?php
          $exhibitor = get_field('exhibitor');
          $location = get_field('location');
          $year = get_field('year');
        ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <?php the_title(); ?>
          <?php if ($exhibitor) { ?>
            <br><?= $exhibitor ?>
          <?php } ?>        
          <?php if ($location) { ?>
            <br><?= $location ?>
          <?php } ?>       
          <?php if ($year) { ?>
            <br><?= $year ?>
          <?php } ?>
        </a>
      </div>
    </div>

  <?php endwhile; ?>
  <?php endif; ?>
</div>

<div class="exhibitions-text">
  <?= get_field('exhibitions_content', 'option') ?>
</div>

<?php get_template_part('partials/pagination'); ?>

<?php get_footer(); ?>
