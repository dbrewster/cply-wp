<?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <?php if ( has_post_thumbnail()) : ?>
      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php the_post_thumbnail(array(120,120)); ?>
      </a>
    <?php endif; ?>

<?php endwhile; ?>

<?php else: ?>

  NOTHING TO DISPLAY

<?php endif; ?>
