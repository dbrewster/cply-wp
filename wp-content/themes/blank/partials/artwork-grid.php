<div class="image-grid">
  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <?php 

      $image = get_field('image');
      $width = $image['sizes']['large-width'];
      $height = $image['sizes']['large-height'];
      $padding = $height / $width * 100 . '%';
      $i = $wp_query->current_post; 

      $url = get_permalink();
      $type = get_query_var('type');

      if ($type) {
        $url = add_query_arg('type', $type, $url);
      }

      if (isset($wp_query->query_vars['artwork_query_args'])) {
        foreach ($wp_query->query_vars['artwork_query_args'] as $key=>$val) {
          $url = add_query_arg($key, $val, $url);
        }
      }
    ?>

    <div class="<?= $post->post_type; ?>-single image-preview <?= clear_classes($i); ?>">
      <a title="<?php the_title(); ?>" class="lightbox-link" href="<?= $url; ?>" style="padding-bottom:<?= $padding ?>">
        <img src="<?= $image['sizes']['medium']; ?>" data-width=<?= $width; ?> data-height=<?= $height; ?>>
        <div class="image-preview__overlay">
          <?= $post->post_title; ?>
        </div>
      </a>
    </div>

  <?php endwhile; ?>
  <?php endif; ?>
</div>
