<?
  $thumb_id = get_post_thumbnail_id($post->ID);
  $img = wp_get_attachment_image_src($thumb_id, 'large');
  $width = $img[1];
  $height = $img[2];
  $padding = $height / $width * 100 . '%';
?>

<div class="news-item-summary">
  <div class="news-item-summary__image image-preview">
    <?php if (has_post_thumbnail()) : ?>
      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" style="padding-bottom: <?= $padding; ?>">
        <?php the_post_thumbnail('large'); ?>
      </a>
    <?php endif; ?>
  </div>
  <div class="news-item-summary__meta">
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
      <?php the_title(); ?>
    </a>
  </div>
</div>