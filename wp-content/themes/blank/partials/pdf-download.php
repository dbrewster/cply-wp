<!-- PDF Container start -->
<?php 
  $file = get_field('file');
  $thumbnail = get_field('thumbnail');
?>
<?php if ($file) { ?>
  <div class="pdf-download">
    <?php if ($thumbnail) { ?>
      <div class="pdf-download__preview">
        <a href="<?= $file['url'] ?>" target="_blank" download>
          <img src="<?= $thumbnail['url'] ?>">
        </a>
      </div>
    <?php } ?>
    
    <a class="pdf-download__link" href="<?= $file['url'] ?>" target="_blank" download>Download PDF</a>
  </div>
<?php } ?>
<!-- PDF Container end -->