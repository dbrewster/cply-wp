<?php
  $artist = get_field('artist');
  $title = get_field('title');
  $subtitle = get_field('subtitle');
  $gallery_name = get_field('gallery_name');
  $year = get_field('year');
  $medium = get_field('medium');
  $dimensions = get_field('dimensions');
  $collection_of = get_field('collection_of');
  $venue = get_field('venue');
  $issue = get_field('issue');
  $photo_credit = get_field('photo_credit');
?>

<?php if ($subtitle) { ?>
  <div class="artwork-subtitle"><?= $subtitle ?></div>
<? } ?>

<div class="caption__secondary">
  <?php if ($artist) { ?>
    <div class="artwork-artist"><?= $artist ?></div>
  <? } ?>
  <?php if ($title) { ?>
    <div class="artwork-title"><?= $title ?></div>
  <? } ?>
  <?php if ($gallery_name) { ?>
    <div class="artwork-gallery-name"><?= $gallery_name ?></div>
  <? } ?>
  <?php if ($year) { ?>
    <div class="artwork-year"><?= $year ?></div>
  <? } ?>
  <?php if ($medium) { ?>
    <div class="artwork-medium"><?= $medium ?></div>
  <? } ?>
  <?php if ($dimensions) { ?>
    <div class="artwork-dimensions"><?= $dimensions ?></div>
  <? } ?>
  <?php if ($collection_of) { ?>
    <div class="artwork-collection-of"><?= $collection_of ?></div>
  <? } ?>
  <?php if ($venue) { ?>
    <div class="artwork-venue"><?= $venue ?></div>
  <? } ?>
  <?php if ($issue) { ?>
    <div class="artwork-issue"><?= $issue; ?></div>
  <? } ?>
  <?php if ($photo_credit) { ?>
    <div class="artwork-photo-credit"><i><?= $photo_credit ?></i></div>
  <? } ?>

  <?php if (has_term('unlocated-paintings', 'type')) { ?>
    <br><br>
    Do you know where this painting is?<br>
    <a href="<?= site_url(); ?>/catalogue-raisonne/questionnaire/">Yes</a>
  <?php } ?>
</div>