<?php

if (is_page() || is_singular('copley-images')) {
  $q = new WP_Query();
  $all_wp_pages = $q->query(array('post_type' => 'page', 'posts_per_page' => -1, 'orderby' => 'menu_order'));

  if (is_singular('copley-images')) {
    $parent = get_page_by_title('William N. Copley')->ID;
  } else if (!empty($post->post_parent)) {
    $parent = $post->post_parent;
  } else {
    $parent = $post->ID;
  }

  $pages = get_page_children($parent, $all_wp_pages);
  $pages = array_reverse($pages);

  $children = [];

  foreach ($pages as $page) {
    $url = null;

    if ($page->post_name == 'images') { // Copley images
      $q = new WP_Query(array('posts_per_page' => -1, 'post_type'=>'copley-images'));
      $images = $q->get_posts();

      if (count($images) > 0) {
        $url = get_permalink($images[0]->ID);
        $current = is_singular('copley-images');
      }
    } else {
      $url = get_permalink($page->ID);
      $current = $post->post_name == $page->post_name;
    }

    if (isset($url)) {
      array_push($children, array(
        'label' => $page->post_title,
        'url' => $url,
        'current' => $current
      ));
    }
  }
} else if ($post->post_type == 'artwork' && !is_single()) {
  $terms = get_terms('type');

  if (isset($wp_query->query_vars['type'])) {
    $current = $wp_query->query_vars['type'];
  } else {
    $current = null;
  }

  $children = [];

  foreach ($terms as $item) {
    if ($item->slug != 'unlocated-paintings') {
      array_push($children, array(
        'label' => $item->name,
        'url' => get_site_url() . '/artwork?type=' . $item->slug,
        'current' => $item->slug == $current
      ));
    }
  }

  array_push($children, array(
    'label' => 'Overview',
    'url' => get_site_url() . '/artwork',
    'current' => !$current
  ));
}

if (isset($children)) { ?>
  <ul class="section-subnav">
    <?php foreach ($children as $child) { ?>
      <li class="section-subnav__item <?php if ($child['current']) { echo 'section-subnav__item--current'; }; ?>">
        <a href="<?= $child['url'] ?>"><?= $child['label']; ?></a>
      </li>
    <?php } ?>
  </ul>
<?php } ?>