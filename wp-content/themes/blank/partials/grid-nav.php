<nav class="grid-nav">
  <ul>
    <li class="grid-nav__item grid-nav__item--logo" data-page="index">
      <div class="grid-nav__item__spacer">
        <img src="<?= get_template_directory_uri(); ?>/img/logo.png">
      </div>
    </li>
    <li class="grid-nav__item grid-nav__item--about" data-page="about">
      <div class="grid-nav__item__spacer"></div>
      <a href="<?= site_url(); ?>/about/biography" class="grid-nav__item__link">William N. Copley</a>
    </li>
    <li class="grid-nav__item grid-nav__item--artwork" data-page="artwork">
      <div class="grid-nav__item__spacer"></div>
      <a href="<?= site_url(); ?>/artwork?type=selected-paintings" class="grid-nav__item__link">Artwork</a>
    <li class="grid-nav__item grid-nav__item--news clear-4 clear-3" data-page="news">
      <div class="grid-nav__item__spacer"></div>
      <a href="<?= site_url(); ?>/news" class="grid-nav__item__link">News</a>
    </li>
    <li class="grid-nav__item grid-nav__item--catalogue" data-page="catalogue-raisonne">
      <div class="grid-nav__item__spacer"></div>
      <a href="<?= site_url(); ?>/catalogue/mission" class="grid-nav__item__link">Catalogue Raisonné</a>
    </li>
    <li class="grid-nav__item grid-nav__item--exhibitions" data-page="exhibitions">
      <div class="grid-nav__item__spacer"></div>
      <a href="<?= site_url(); ?>/exhibitions" class="grid-nav__item__link">Exhibitions</a>
    </li>
    <li class="grid-nav__item grid-nav__item--estate clear-3" data-page="estate">
      <div class="grid-nav__item__spacer"></div>
      <a href="<?= site_url(); ?>/estate" class="grid-nav__item__link">Estate</a>
    </li>
    <li class="grid-nav__item grid-nav__item--galleries clear-4" data-page="programs">
      <div class="grid-nav__item__spacer"></div>
      <a href="<?= site_url(); ?>/programs/copley-galleries" class="grid-nav__item__link">Copley Galleries, Foundation & The Letter Edged In Black Press</a>
    </li>
  </ul>

  <?php if (get_field('show_homepage_news', 'option')) { ?>
    <?php
      $opts = array('post_type'=>'news', 'numberposts'=>1); 
      $post = get_posts($opts)[0];
      setup_postdata($post);
      get_template_part('partials/news-item-summary');
      wp_reset_postdata();
    ?>
  <?php } ?>
</nav>
