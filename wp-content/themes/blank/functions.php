<?php

/*------------------------------------*\
  External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
  Theme Support
\*------------------------------------*/

if (function_exists('add_theme_support'))
{
  // Add Menu Support
  add_theme_support('menus');

  // iAdd Thumbnail Theme Support
  add_theme_support('post-thumbnails');
  add_image_size('large', 2000, '', true); // Large Thumbnail
  add_image_size('medium', 400, '', true); // Medium Thumbnail
  add_image_size('small', 120, '', true); // Small Thumbnail
  add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
}

/*------------------------------------*\
  ACF Setup
\*------------------------------------*/

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
}

/*------------------------------------*\
  Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
  wp_nav_menu(
  array(
      'theme_location'  => 'header-menu',
      'menu'            => '',
      'container'       => 'div',
      'container_class' => 'menu-{menu slug}-container',
      'container_id'    => '',
      'menu_class'      => 'menu',
      'menu_id'         => '',
      'echo'            => true,
      'fallback_cb'     => 'wp_page_menu',
      'before'          => '',
      'after'           => '',
      'link_before'     => '',
      'link_after'      => '',
      'items_wrap'      => '<ul>%3$s</ul>',
      'depth'           => 0,
      'walker'          => ''
      )
  );
}

// Get favicon for current page
function get_favicon () {
  global $post;

  if (is_home()) {
    $iconName = 'home';
  } elseif (is_page()) {
    $page = $post;
    if ($page->post_parent) {
      $page = get_post($page->post_parent);
    }

    switch ($page->post_name) {
      case 'catalogue-raisonne': 
        $iconName = 'catalogue';
        break;
      case 'programs':
        $iconName = 'programs';
        break;
      case 'estate':
        $iconName = 'estate';
        break;
      case 'about':
        $iconName = 'about';
        break;
      default: 
        $iconName = 'home';
    }
  } elseif (is_archive() || is_single()) {
    $iconName = $post->post_type;
  } else {
    $iconName = 'home';
  }

  return get_template_directory_uri() . '/img/favicons/icon-' . $iconName . '.ico';
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts() {
  if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
    wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
    wp_enqueue_script('conditionizr');

    wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
    wp_enqueue_script('modernizr');

    wp_register_script('imagesloaded', get_template_directory_uri() . '/js/lib/imagesloaded.pkgd.min.js', array('jquery'), '1.0.0'); // Imagesloaded
    wp_enqueue_script('imagesloaded');

    wp_register_script('main', get_template_directory_uri() . '/js/app.js', array('jquery'), '1.0.0'); // App scripts
    wp_enqueue_script('main');
  }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
  wp_register_style('reset', get_template_directory_uri() . '/css/reset.css', array(), '1.0', 'all');
  wp_enqueue_style('reset'); // Enqueue it!

  wp_register_style('main', get_template_directory_uri() . '/css/main.css', array(), '1.0', 'all');
  wp_enqueue_style('main'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
  register_nav_menus(array( // Using array to specify more menus if needed
      'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
      'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
      'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
  ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
  $args['container'] = false;
  return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
  return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
  return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
  global $post;
  if (is_home()) {
    $key = array_search('blog', $classes);
    if ($key > -1) {
        unset($classes[$key]);
    }
  } elseif (is_page()) {
    $classes[] = sanitize_html_class($post->post_name);

    $parentId = $post->post_parent;
    if ($parentId) {
      $parent = get_post( $parentId );
      $classes[] = sanitize_html_class($parent->post_name);
    }
  } elseif (is_singular()) {
    $classes[] = sanitize_html_class($post->post_name);
  }

  return $classes;
}

// For now only adds 'initializing', to be removed a bit after page load
function add_extra_body_classes($classes) {
  $classes[]= 'initializing';
  return $classes;
}

function clear_classes($counter) {
  $clear_intervals = [3, 4];
  $clear = [];

  foreach ($clear_intervals as $interval) {
    if ($counter != 0 && ($counter  + 1) % $interval == 0) {
      array_push($clear, 'clear-' . $interval);
    } 
  }

  return implode(' ', $clear);
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
  // Define Sidebar Widget Area 1
  register_sidebar(array(
      'name' => __('Widget Area 1', 'html5blank'),
      'description' => __('Description for this widget-area...', 'html5blank'),
      'id' => 'widget-area-1',
      'before_widget' => '<div id="%1$s" class="%2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h3>',
      'after_title' => '</h3>'
  ));

  // Define Sidebar Widget Area 2
  register_sidebar(array(
      'name' => __('Widget Area 2', 'html5blank'),
      'description' => __('Description for this widget-area...', 'html5blank'),
      'id' => 'widget-area-2',
      'before_widget' => '<div id="%1$s" class="%2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h3>',
      'after_title' => '</h3>'
  ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
  global $wp_widget_factory;
  remove_action('wp_head', array(
      $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
      'recent_comments_style'
  ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
  global $wp_query;
  $big = 999999999;
  echo paginate_links(array(
      'base' => str_replace($big, '%#%', get_pagenum_link($big)),
      'format' => '?paged=%#%',
      'current' => max(1, get_query_var('paged')),
      'total' => $wp_query->max_num_pages
  ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
  return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
  return 40;
}

// Get title of current section
function get_section_title() {
  global $post;
  global $wp_query;

  if (is_page()) {
    $parentId = $post->post_parent;
    if ($parentId) {
      $section_post = get_post( $parentId );
    } else {
      $section_post = $post;
    }

    return $section_post->post_title;
  } else if (is_singular('artwork')) {
    return '';
  } else if (is_singular('copley-images')) {
    return 'William N. Copley';
  } else if (!empty($wp_query->query_vars['post_type'])){
    $type = get_post_type_object($wp_query->query_vars['post_type']);
    $label = $type->labels->name;
    $link = get_post_type_archive_link($type->name);

    if (is_single()) {
      $label = 'Back to ' . $label;
      return "<a href='" . $link . "'>" . $label . "</a>";
    } else {
      return $label;
    }
  }
}

// Get title of current page
function get_page_title() {
  global $post;
  global $wp_query;

  if (is_page() && $post->post_parent) {
    return $post->post_title;
  } else if (get_post_type() == 'artwork') {
    $type = get_query_var('type');

    if (empty($type)) {
      return 'Overview';
    } else {
      $term = get_term_by('slug', $type, 'type');
      return $term->name;
    }
  }
}

function get_page_type() {
  global $post;

  if (is_home()) {
    return 'index';
  } else if (is_page()) {
    $parentId = $post->post_parent;
    if ($parentId) {
      $section_post = get_post( $parentId );
    } else {
      $section_post = $post;
    }

    return $section_post->post_name;
  } else if (is_singular('copley-images')) {
    return 'about';
  } else {
    return get_post_type();
  }
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
  global $post;
  if (function_exists($length_callback)) {
      add_filter('excerpt_length', $length_callback);
  }
  if (function_exists($more_callback)) {
      add_filter('excerpt_more', $more_callback);
  }
  $output = get_the_excerpt();
  $output = apply_filters('wptexturize', $output);
  $output = apply_filters('convert_chars', $output);
  $output = '<p>' . $output . '</p>';
  echo $output;
}

// Remove Admin bar
function remove_admin_bar()
{
  return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
  return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
  $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
  return $html;
}

// Limit the number of news posts returned
function limit_news_posts( $query ) {
  if (!is_admin() && $query->is_main_query() && is_post_type_archive('news')) {
    $query->set('posts_per_page', '10');
  }
}

// Add year and order to artwork meta query
function add_artworks_meta($query) {
  $isArtworkQuery = get_post_type() == 'artwork' ||
    is_post_type_archive('artwork') ||
    (isset($query->query_vars['post_type']) &&
      $query->query_vars['post_type'] == 'artwork');

  if (!is_admin() && ($query->is_main_query() || is_page('unlocated-paintings')) && $isArtworkQuery) {
    $query->set('meta_query', array(
      array(
        'key' => 'year'
      ),
      array(
        'key' => 'order',
      )
    ));

    add_filter('posts_orderby','order_artworks');
  }
}

// Set orderby for artworks
function order_artworks($orderby) {
  remove_filter('posts_orderby','order_artworks');
  return 'wp_postmeta.meta_value+0 ASC, mt1.meta_value+0 ASC';
}

function get_offset_artwork_permalink($offset) {
  add_filter('posts_orderby','order_artworks');

  $args = array(
    'post_type' => 'artwork',
    'posts_per_page' => -1,
    'meta_query' => array(
      array(
        'key' => 'year'
      ),
      array(
        'key' => 'order'
      )
    )
  );

  $type = get_query_var('type');

  if ($type) {
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'type',
        'field' => 'slug',
        'terms' => $type
      )
    );
  }

  $q = new WP_Query($args);

  $artworks = $q->posts;
  $curId = get_the_ID();

  foreach ($artworks as $index=>$artwork) {
    if ($artwork->ID == $curId) {
      $curIndex = $index;
    }
  }

  if (array_key_exists($curIndex + $offset, $artworks) !== true) {
    return;
  }

  $nextArtwork = $artworks[$curIndex + $offset];

  if ($nextArtwork) {
    $link = get_the_permalink($nextArtwork->ID);

    if ($link && $type) {
      $link .= '?type=' . $type;
    }

    return $link;
  }
}

// Remove unlocated from artwork archive queries
function exclude_unlocated_from_artwork_archive($query) {
  if (!is_admin() && $query->is_main_query() && is_post_type_archive('artwork')) {
    $query->set('tax_query', array( 
        array( 
          'taxonomy' => 'type', 
          'field' => 'slug',
          'terms' => array('unlocated-paintings'),
          'operator' => 'NOT IN'
        )
      )
    );
  }
}

// Regenerate thumb on image save
function add_image_options($data){
  global $_wp_additional_image_sizes;
  foreach($_wp_additional_image_sizes as $size => $properties){
    update_option($size."_size_w", $properties['width']);
    update_option($size."_size_h", $properties['height']);
    update_option($size."_crop", $properties['crop']);
  }
  return $data;
}

function add_order_meta_keys () {
  $posts = get_posts(array('post_type'=>'artwork', 'posts_per_page'=>-1));

  foreach ($posts as $post) {
    add_post_meta($post->ID, 'order', 0, true);
  }
}

// add_order_meta_keys();

/*------------------------------------*\
  Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
add_action('pre_get_posts', 'limit_news_posts' );
add_action('pre_get_posts', 'add_artworks_meta');
add_action('pre_get_posts', 'exclude_unlocated_from_artwork_archive');
add_action('image_save_pre', 'add_image_options');

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('body_class', 'add_extra_body_classes'); // Add assorted extra classes to body
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

