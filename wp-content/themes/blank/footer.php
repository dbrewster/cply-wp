        <!-- Inner wrapper end -->
        </div>

      <!-- Grid wrapper end -->
      </div>
      
      <?php wp_footer(); ?>

      <?php if ((!is_single() && !is_home()) || get_post_type() == 'exhibitions' || get_post_type() == 'news') { ?>
        <div class="footer">
          <div class="footer__content">
            &copy; <a href="<?= get_site_url(); ?>/estate">William N. Copley Estate</a>. Represented by <a href="http://paulkasmingallery.com/artists/william-n-copley" target="_blank">Paul Kasmin Gallery</a>.
          </div>
        </div>
      <?php } ?>

      <!-- Colored background end -->
      </div>

    <!-- Page wrapper end -->
    </div>

    <!-- analytics -->
    <script>
      (function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
      (f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
      l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
      ga('send', 'pageview');
    </script>

  </body>
</html>
