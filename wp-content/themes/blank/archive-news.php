<?php get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>
  
  <div class="news-content">
    <?php get_template_part('partials/news-item-summary'); ?>
  </div>

<?php endwhile; ?>
<?php endif; ?>

<?php get_template_part('partials/pagination'); ?>

<?php get_footer(); ?>
