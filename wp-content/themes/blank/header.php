<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title>William N. Copley</title>

    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="<?= get_favicon(); ?>" rel="shortcut icon">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <?php wp_head(); ?>
    <script>
      // configure environment tests
      conditionizr.config({
        assets: '<?php echo get_template_directory_uri(); ?>',
        tests: {}
      });
    </script>

  </head>

  <body class="<?= get_page_type(); ?>-color">

    <!-- Page wrapper start -->
    <div class="page-wrapper <?= implode(get_body_class(), ' ');?>" data-section="<?= get_page_type(); ?>">

      <!-- Colored background start -->
      <div class="bg-wrapper">

      <!-- Grid wrapper start -->
      <div class="outer-wrapper">

        <?php if (!is_home()) { ?>
          <div class="header">
            <div class="header__left-column">
              <div class="header__section-title"><?= get_section_title(); ?></div>

              <?php get_template_part('partials/section-subnav'); ?>
            </div>

            <div class="header__right-column">
              <?php if (!is_single() || is_singular('copley-images')) { ?>
                <a class="header__menu-link" href="<?= site_url(); ?>"><img src="<?= get_template_directory_uri(); ?>/img/logo.png"></a>
              <?php } ?>
            </div>

            <div class="header__center-column">
              <?php $title = get_page_title(); ?>
              <?php if (!is_single() && !empty($title)) { ?>
                <div class="header__page-title"><?= $title; ?></div>
              <?php } ?>

              <?php if (is_page() && $post->post_parent && $title !== "Unlocated Paintings") { ?>
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                  <div class="page-content">
                    <?php get_template_part('partials/pdf-download'); ?>
                    <?php the_content(); ?>
                  </div>
                <?php endwhile; endif; ?>
              <?php } ?>
            </div>
          </div>
        <? } ?>

        <!-- Inner wrapper start -->
        <div class="inner-wrapper">
