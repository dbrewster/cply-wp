var pagination = {

  init: function () {
    $(document).on('click', '.pagination a', function () {
      var url = $(this).attr('href');

      $.get(url, function (res) {
        $('.pagination').remove();
        $('.inner-wrapper').append($(res).find('.inner-wrapper').html());
      });

      return false;
    });
  }

}

module.exports = pagination;
