var gridNav = {
  
  init: function () {
    var _this = this;
  
    $(window).on('resize', this.truncateLinks);
    $(window).on('pageload', function () {
      _this.setup();
    });

    this.setup();
  },

  setup: function () {
    this.processLinks();
    this.truncateLinks();
  },

  processLinks: function () {
    $('.grid-nav .grid-nav__item__link').each(function () {
      var $link = $(this);
      var text = $link.text();
      var words = text.split(' ');

      $link.html('');

      var finalWords = [];

      for (var i = 0; i < words.length; ++i) {
        finalWords.push('<span>' + words[i] + '</span>');
      }

      $link.html(finalWords.join(' '));
      $('<span class="ellipsis">...</span>').hide().appendTo($link);
    });
  },

  truncateLinks: function () {
    $('.grid-nav .grid-nav__item__link').each(function () {
      var hasHidden = false;
      var bottom = this.getBoundingClientRect().bottom;

      $(this).find('span').each(function () {
        $(this).show();

        var wordBottom  = this.getBoundingClientRect().bottom;

        if (wordBottom > bottom) {
          $(this).hide()
          hasHidden = true;
        }
      });

      var $ellipsis = $(this).find('.ellipsis');
      if (hasHidden) {
        $ellipsis.show();

        var ellipsisBottom = $ellipsis[0].getBoundingClientRect().bottom;
        while (ellipsisBottom > bottom) {
          $(this).find('span:not(.ellipsis):visible:last').hide();
          ellipsisBottom = $ellipsis[0].getBoundingClientRect().bottom;
        }
      } else {
        $ellipsis.hide();
      }
    });
  }



}

module.exports = gridNav;
