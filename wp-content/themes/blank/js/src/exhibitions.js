var exhibitions = {

  init: function () {
    if ($('.page-wrapper').hasClass('single-exhibitions')) {
      var $meta = $('.exhibition__meta');

      $('.page-wrapper').on('scroll.exhibitions', setNavState);

      $(window).on('resize.exhbitions', function () {
        $meta.removeClass('exhibition__meta--fixed').attr('css', '');
        setNavState();
      });
    } else {
      $('.page-wrapper').off('scroll.exhibitions');
      $(window).off('resize.exhibitions');
    }

    function setNavState () {
      var wW = $(window).width();
      var top = $('.page-wrapper').scrollTop();
      var offset = (wW - $('.outer-wrapper').width()) / 2; 

      if (wW < 600) {
        return;
      }

      if (top > $('.exhibition__images').position().top - offset) {
        $meta.css({
          left: $meta.offset().left,
          top: offset
        }).addClass('exhibition__meta--fixed')
      } else {
        $meta.removeClass('exhibition__meta--fixed').attr('css', '')
      }
    }
  }

}

module.exports = exhibitions;
