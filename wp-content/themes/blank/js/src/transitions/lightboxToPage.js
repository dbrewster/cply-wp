var util = require('../util');

var lightboxToPage = function (e, done) {
  var url = this.href;
  var oldUrl = window.location.href;

  var $lightboxImage = $('.lightbox').find('img');
  var lightboxImageOffset = $lightboxImage[0].getBoundingClientRect();

  $lightboxImage.css({
    position: 'fixed',
    top: lightboxImageOffset.top,
    left: lightboxImageOffset.left,
    width: lightboxImageOffset.width,
    height: lightboxImageOffset.height,
    zIndex: 2,
    margin: 0
  }).appendTo('body');

  var $mask = $('.page-wrapper').css({
    position: 'fixed',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 2
  });

  util.loadPage(url, function (data) {
    util.setupPage(data, false);

    var $nextPage = $(data).filter('.page-wrapper');
    $('body').append($nextPage);

    $mask.fadeOut(200, function () {
      $mask.remove();
    });

    var $src = $nextPage.find('a[href="' + oldUrl + '"]');
    $src.css({
      opacity: 0
    });

    var wH = $(window).height();
    var srcH = $src.outerHeight();
    var srcOffset = $src[0].getBoundingClientRect();
    var scrollTop = ((wH - srcH) / 2);

    $('.page-wrapper').scrollTop(srcOffset.top - ((wH - srcH) / 2));

    var $srcOffset = $src[0].getBoundingClientRect();

    $lightboxImage.animate({
      top: $srcOffset.top,
      left: $srcOffset.left,
      width: $srcOffset.width,
      height: $srcOffset.height
    }, 200, function () {
      $src.css({
        opacity: 1
      });
    });

    $src.imagesLoaded(function () {
      $lightboxImage.fadeOut(function () {
        $lightboxImage.remove();
        done();
      });
    });

  });
}

module.exports = lightboxToPage;
