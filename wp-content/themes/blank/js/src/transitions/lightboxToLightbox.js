var util = require('../util');

var lightboxLinkTimer;

var lightboxToLightbox = function (e, done) {
  var url = $(this).attr('href');

  if (lightboxLinkTimer) clearTimeout(lightboxLinkTimer);

  if ($(this).is('.lightbox__prev_link a')) {
    $('html').addClass('prev-clicked');
  } else {
    $('html').addClass('next-clicked');
  }

  var $el = $('.page-wrapper');

  $el.css({
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    zIndex: 2
  });

  util.loadPage(url, function (data) {
    var $nextPage = $(data).filter('.page-wrapper');

    $nextPage.find('.lightbox__item').css({
      opacity: 0
    });

    $('body').append($nextPage);

    util.setupPage(data, false);

    $nextPage.imagesLoaded(function () {
      $nextPage.find('.lightbox__item').animate({
        opacity: 1
      });

      $el.fadeOut(function () {
        $el.remove();
        done();
      });
    });
  });
}

module.exports = lightboxToLightbox;
