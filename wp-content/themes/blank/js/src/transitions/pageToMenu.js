var util = require('../util');

var pageToMenu = function (e, done) {
  var $el = $('.page-wrapper');
  var url = $(this).attr('href');

  $el.css({
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    zIndex: 2
  });

  $(['.footer',
    '.header__center-column',
    '.header__right-column',
    '.section-subnav',
    '.page-content',
    '.inner-wrapper'].join(',')).animate({
      opacity: 0
    }, 200);

  setTimeout(loadMenu, 200);

  function loadMenu () {
    util.loadPage(url, function (data) {
      util.setupPage(data, false);

      var $nextPage = $(data).filter('.page-wrapper').appendTo('body')

      var section = $el.data('section');
      var $target = $nextPage.find('.grid-nav__item[data-page=' + section + ']');

      var offset = $target[0].getBoundingClientRect();
      var $dummy = $target.clone();

      $dummy.find('.grid-nav__item__link').css({
        width: $target.width() - 44
      });

      $dummy.addClass('is-shrinking').css({
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        opacity: 0,
        zIndex: 1
     }).appendTo('body');

      $dummy.animate({
        opacity: 1
      }, 200, function () {
        $el.remove();

        $dummy.animate({
          top: offset.top,
          left: offset.left,
          width: $target.width(),
          height: $target.height()
        }, function () {
          $dummy.remove();
          done();
        });
      })

    });
  }
}

module.exports = pageToMenu;