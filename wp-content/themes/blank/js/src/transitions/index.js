var util = require('../util');

var transitioning = false;

var transitions = {

  types: {
    lightboxToPage: require('./lightboxToPage'),
    menuToPage: require('./menuToPage'),
    pageToLightbox: require('./pageToLightbox'),
    pageToMenu: require('./pageToMenu'),
    simpleFade: require('./simpleFade'),
    lightboxToLightbox: require('./lightboxToLightbox')
  },

  init: function () {
    var _this = this;

    // No transitions in older browsers
    if (!window.history || !window.history.replaceState) return;

    $(document).on('click', 'a', function (e) {
      // No transitions for external links
      var href = $(this).attr('href');
      if (!href || href == '' ||
        util.isExternalUrl(href) ||
        util.isFileUrl(href) ||
        util.isMailtoUrl(href)) return;

      var selectorMap = {
        '.grid-nav__item:not(.grid-nav__item--logo) a': _this.types.menuToPage,
        '.header__menu-link': _this.types.pageToMenu,
        '.lightbox-link': _this.types.pageToLightbox,
        '.lightbox__nav_link a': _this.types.lightboxToLightbox,
        '.lightbox__close_link:not(.single-copley-images a)': _this.types.lightboxToPage
      };

      var transitionFn;

      for (var selector in selectorMap) {
        if ($(this).is(selector)) {
          transitionFn = selectorMap[selector];
        }
      }

      if (!transitionFn) transitionFn = _this.types.simpleFade;

      if (transitionFn) {
        if (!transitioning) {
          util.startLoad();

          transitioning = true;

          transitionFn.apply(this, [e, function () {
            transitioning = false;
          }]);
          history.pushState({ added: true }, 'CPLY', this.href);
        }

        return false;
      }
    });

    window.onpopstate = function (e) {
      if (e.state && e.state.added) {
        window.location.reload();
      }
    }

    history.replaceState({ added: true }, 'CPLY');
  }

}

module.exports = transitions;
