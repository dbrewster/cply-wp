var util = require('../util');

var pageToLightbox = function (e, done) {
  var $el = $(this);
  var $image = $(this).find('img');
  var $wrapper = $('.outer-wrapper');

  var lightboxTemplate = '<div class="lightbox--dummy"><div class="lightbox__inner"><div class="lightbox__item"><div class="lightbox__item__image"></div></div></div></div>';
  var $dummyLightbox = $(lightboxTemplate).appendTo('.inner-wrapper');
  var $dummyImage = $dummyLightbox.find('.lightbox__item__image');
  var containerOffset = $dummyImage[0].getBoundingClientRect();
  var containerHeight = Math.ceil(containerOffset.height);
  var containerWidth = Math.ceil(containerOffset.width);
  $dummyLightbox.remove();

  var offset = this.getBoundingClientRect();
  var aspect = offset.width / offset.height;
  var imgW = $image.data('width');
  var imgH = $image.data('height');

  var maxW = imgW > containerWidth ? containerWidth : imgW;
  var maxH = imgH > containerHeight ? containerHeight : imgH;

  var w = maxW;
  var h = Math.ceil(w / aspect);

  if (h > maxH) {
    h = maxH;
    w = h * aspect;
  }

  w = Math.ceil(w);
  h = Math.ceil(h);

  var $dummyImage = $image.clone().css({
    position: 'fixed',
    top: offset.top,
    left: offset.left,
    width: offset.width,
    height: offset.height,
    zIndex: 10
  });

  $('body').append($dummyImage);

  $image.css({
    opacity: 0
  });

  var $mask = $('<div>').css({
    position: 'fixed',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    background: '#FFF',
    opacity: 0,
    zIndex: 1
  }).appendTo('body').animate({
    opacity: 1
  }, 200);

  $dummyImage.animate({
    width: w,
    height: h,
    top: containerOffset.top + (containerHeight - h) / 2,
    left: containerOffset.left + (containerWidth - w) / 2
  }, 200, function () {
    util.loadPage($el.attr('href'), function (data) {
      util.setupPage(data);

      $('body').append($dummyImage);

      $('.lightbox__item, .lightbox__close_link, .lightbox__item__caption').css({
        opacity: 0
      });

      $('.lightbox__item').imagesLoaded(function () {
        $('.lightbox__item').css({
          opacity: 1
        });

        $mask.remove();
        $dummyImage.remove();
        done();
      });

      $('.lightbox__close_link, .lightbox__item__caption').animate({
        opacity: 1
      }, 200);
    });
  });
}

module.exports = pageToLightbox;
