var util = require('../util');

var menuToPage = function (e, done) {
  var $lastPage = $('.page-wrapper');
  var $el = $(this).closest('.grid-nav__item').clone();
  var offset = this.getBoundingClientRect();
  var url = $(this).attr('href')

  $el.css({
    position: 'fixed',
    top: offset.top - 2,
    left: offset.left - 2,
    width: offset.width + 4,
    height: offset.height + 4,
    zIndex: 2
  });

  $el.find('.grid-nav__item__link').css({
    width: offset.width - 40
  });

  $el.appendTo('body');

  setTimeout(function () {
    $el.addClass('is-enlarging');
  });

  $el.animate({
    top: 0,
    left: 0,
    width: '100%',
    height: '100%'
  }, 500, function () {
    if (!history) {
      window.location.href = url;
      return;
    }

    $lastPage.remove();

    util.loadContentUnderMask(url, $el, function () {
      done();
    });
  });

  return false;
}

module.exports = menuToPage;
