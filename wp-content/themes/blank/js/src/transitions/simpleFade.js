var util = require('../util');

var lightboxLinkTimer;

var simpleFade = function (e, done) {
  var url = $(this).attr('href');

  var $nextMenuItem = $('a[href="' + url + '"]').closest('.section-subnav__item');

  if ($nextMenuItem.length) {
    $('.section-subnav__item').removeClass('section-subnav__item--current');
    $nextMenuItem.addClass('section-subnav__item--current');
  }

  var toHide = [
    '.inner-wrapper',
    '.page-content',
    '.header__page-title'
  ];

  if ($('.page-wrapper').is('.post-type-archive-exhibitions, .post-type-archive-news')) {
    toHide.push('.header__right-column');
    toHide.push('.header__section-title');
  }

  if ($('.page-wrapper').is('.single-exhibitions, .single-news')) {
    toHide.push('.header__section-title');
  }

  $(toHide.join(', ')).animate({
    opacity: 0
  }, 200);

  util.resetScroll(function () {
    var $el = $('.page-wrapper');

    $el.css({
      position: 'fixed',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      zIndex: 1
    });

    util.loadContentUnderMask(url, $el, done);
  });
}

module.exports = simpleFade;
