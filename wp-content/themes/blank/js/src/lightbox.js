var lightbox = {

  init: function () {
    var open = false;

    $(document).on('click', '.lightbox__info_link', function () {
      var $caption = $('.lightbox__mobile_caption');

      if (open) {
        $caption.animate({
          height: 0
        }, 200);

        open = false;
      } else {
        $caption.css({
          height: 'auto'
        });

        var height = $caption.height();

        $caption.css({ height: 0 }).animate({
          height: height
        }, 200);

        open = true;
      }

      return false;
    });

    $(document).on('keyup', function (e) {
      if (e.keyCode == 37 && $('.lightbox__prev_link').length) {
        $('.lightbox__prev_link a').click();
        return false;
      } else if (e.keyCode == 39 && $('.lightbox__next_link').length) {
        $('.lightbox__next_link a').click();
        return false;
      } 
    });
  }

}

module.exports = lightbox;
