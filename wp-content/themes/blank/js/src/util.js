var exhibitions = require('./exhibitions');

var $loader = $('<div class="page-loader"><div class="page-loader__inner">&bullet;</div></div>');

var util = {

  isExternalUrl:  function (url) {
    return url.substring(0,4) == 'http' &&
      !(new RegExp('//' + window.location.host + '($|/)').test(url))
  },

  isFileUrl: function (url) {
    var ext = url.split('.').pop();

    return ext.match(/^(pdf|jpg|png|gif)$/);
  },

  isMailtoUrl: function (url) {
    return !!url.match(/^mailto/);
  },

  loadContentUnderMask: function (url, $mask, cb) {
    var _this = this;

    this.loadPage(url, function (data) {
      _this.setupPage(data, false);
      $(data).filter('.page-wrapper').appendTo('body');

      $mask.fadeOut(200, function () {
        $mask.remove();
        exhibitions.init();
        if (cb) cb();
      });
    });
  },

  startLoad: function () {
    $loader.appendTo('body');
  },

  stopLoad: function () {
    $loader.remove();
  },

  loadPage: function (url, cb) {
    var _this = this;

    $.get(url, function () {
      _this.stopLoad();
      cb.apply(this, arguments);
      $(window).trigger('pageload');
    });
  },

  setupPage: function (pageData, replaceContent) {
    if (arguments.length == 1) replaceContent = true;

    this.updateFaviconFromData(pageData);

    if (replaceContent) {
      this.swapInLoadedContent(pageData);
      exhibitions.init();
    }

    this.setMetaFromLoadedContent(pageData);
    this.setInitializeTimer();
  },

 swapInLoadedContent: function (loadedHtml) {
    $('.page-wrapper').remove();
    $(loadedHtml).filter('.page-wrapper').appendTo('body');
  },

  setMetaFromLoadedContent: function (loadedHtml) {
    var title = loadedHtml.match(/<title>(.+?)<\/title>/)[1];
    var bodyClass = loadedHtml.match(/<body.+?class="(.+?)"/)[1];
    $('title').html(title);
    $('body').attr('class', bodyClass);
  },

 updateFaviconFromData: function (data) {
    var newIcon = $(data).filter('link[rel="shortcut icon"]');
    $('link[rel="shortcut icon"]').replaceWith(newIcon);
  },

 setInitializeTimer: function () {
    setTimeout(function () {
      $('body').removeClass('initializing');
    }, 1000);
  },

  resetScroll: function (cb) {
    $('body').animate({
      scrollTop: 0
    }, cb);
  },

  handleExternalLinks: function () {
    var _this = this;

    $(document).on('click', 'a', function () {
      var url = $(this).attr('href');

      if (_this.isExternalUrl(url)) {
        window.open(url, '_blank');
        return false;
      }
    });
  }

}

module.exports = util;