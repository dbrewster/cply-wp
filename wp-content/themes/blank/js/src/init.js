window.$ = window.jQuery;

var transitions = require('./transitions'),
  pagination = require('./pagination'),
  exhibitions = require('./exhibitions'),
  lightbox = require('./lightbox'),
  gridNav = require('./gridNav'),
  util = require('./util');

$(function () {
  $('img').attr('width', 'auto');
  $('img').attr('height', 'auto');

  pagination.init();
  exhibitions.init();
  transitions.init();
  lightbox.init();
  gridNav.init();
  util.setInitializeTimer();
  util.handleExternalLinks();
});
